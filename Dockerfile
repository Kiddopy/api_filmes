FROM node:8.6-alpine

WORKDIR /

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json ./
COPY . .
COPY ./src** .
RUN npm install

EXPOSE 3000

CMD ["node","server.js"]