# API de Filmes

Este projeto tem como objetivo oferecer uma API de filmes desenvolvida em Node.js.


## Tecnologias Utilizadas

Este projeto conta com as seguintes tecnologias:

- Body Parser (Ver. 1.19.0)
- Express (Ver. 4.17.1)
- Mongoose (Ver. 5.9.16)
- NGinx (Ver. 0.0.0)
- Node (Ver. 12.14.1)
- Docker (Ver. 19.03.8)

As informações são armazenadas em um banco de dados Mongo Atlas.

## Padrões Utilizados

O projeto se organiza da seguinte forma:

**src/app**

- **controllers**: Pasta onde se encontra a classe que deverá realizar a validação dos dados inseridos e envia-los para o banco de dados. 

- **models** : Pasta onde se encontra a classe modelo do filme, desenvolvida com o esquema mongoose.

- **routes** : Pasta onde fica a classe que direciona o método HTTP correspondente com a ação desejada para algum dos métodos da classe controller. 

## Métodos HTTP

Este projeto foi feito para realizar as seguintes requisições HTTP:

**GET**: Método que retorna todas os filmes do banco de dados ou apenas o filme que você especificar por meio do ID. Para isto, basta inseri-lo no final da URL da seguinte forma: [url]/api/filmes/[:id]

**POST**: Método pelo qual você pode inserir informações na API utilizando o seguinte padrão JSON:

        { 
             "nome" : "nome qualquer", 
             "diretor" : "diretor qualquer",
             "genero" : "genero qualquer", 
             "resumo" : "resumo qualquer"
        }

O resultado retornado deverá se paracer com o seguinte:

        { 
             "_id" : "id" 
             "nome" : "nome qualquer", 
             "diretor" : "diretor qualquer",
             "genero" : "genero qualquer", 
             "resumo" : "resumo qualquer"
             "createDate" : "data de criacao"
        }

**PUT** : Método pelo qual você pode editar um determinado filme passando um ID e utilizando o seguinte padrão:

        { 
             "nome" : "nome qualquer", 
             "diretor" : "diretor novo",
             "genero" : "genero novo", 
             "resumo" : "resumo qualquer"
        }

 O resultado retornado deverá se parecer com o seguinte:

        { 
              "_id" : "id" 
              "nome" : "nome qualquer", 
              "diretor" : "diretor novo",
              "genero" : "genero novo", 
              "resumo" : "resumo qualquer"
              "createDate" : "data de criacao"
        }

**PATCH** : Método pelo qual você poderá editar parcialmente um determinado filme, passando, como parâmetro, um ID e seguindo o seguinte padrão de JSON;

        { 
            "nome" : "nome novo qualquer"
        }

 O resultado retornado deverá se parecer com o seguinte:

        { 
             "_id" : "id" 
             "nome" : "nome novo qualquer", 
             "diretor" : "diretor novo",
             "genero" : "genero novo", 
             "resumo" : "resumo qualquer"
             "createDate" : "data de criacao"
        }


**DELETE**: Método pelo qual você poderá deletar um determinado filme passando como parâmetro o ID desejado. 
O resultado retornado deverá se parecer com o seguinte:

        {
            "message":  "Film deleted successfully!" 
        }


## Como executar o projeto

Este projeto foi desenvolvido em Node.js deverá ser executado como um container por meio da ferramenta Docker. Desta forma, é necessário possuir ambos instalados para que ele funcione corretamente. 

### Node.js

Para instalar o Node.js na sua máquina, baixe o instalador no site: https://nodejs.org/en/download/

- Em seguida, execute o instalador e clique em "Next".
- Aceite os termos e condições de uso e clique em "Next" novamente. 
- Escolha o  local de instalação de sua preferência e clique em "Next".
- Marque todas as opções de instalação e clique em "Next";
- Na sequência o instalador pergunta se queremos instalar as ferramentas necessárias para compilar módulos nativos. Não é necessário nesse caso, então clique em "Next".
- Clique em "Install". 

Em seguida, verifique se o Node e o NPM foram instalados corretamente inserindo os seguintes comandos no CMD:

     node --version
     npm --version

### Docker

Caso você não o possua o Docker instalado, siga o passo a passo neste site 

- https://docs.docker.com/docker-for-windows/install/

Em seguida, execute-o.

### Executando o projeto

Verifique se o Docker está funcionando corretamente e, em seguida, clone o projeto por meio do seguinte comando:

- `git clone https://gitlab.com/Kiddopy/api_filmes`

A seguir, abra a pasta e instale as dependências do projeto por meio do CMD utilizando o seguinte comando: 

- `npm install`

Assim que a instalação terminar, execute os seguintes comandos para subir a API:

- `docker build -t api-filmes .`
- `docker run -it --rm -p 9000:3000 api-filmes`

Feito isso, o projeto deverá estar funcionando por meio do seguinte link:

- http://localhost:9000/api/filmes

