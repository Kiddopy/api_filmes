module.exports = (app) => {
    const filmes = require('../controllers/film-controller.js');
    
    app.post('/api/filmes', filmes.create);

    app.get('/api/filmes', filmes.findAll);

    app.get("/api/filmes/:id", filmes.findOne);

    app.put('/api/filmes/:id', filmes.update);

    app.delete('/api/filmes/:id', filmes.delete);

    app.patch('/api/filmes/:id', filmes.partialUpdate);
}