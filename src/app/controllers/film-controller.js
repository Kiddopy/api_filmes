const Filme = require('../models/filme');

exports.create = (req, res) => {
    if(!req.body) {
        return res.status(400).send({
            message: "Film content can not be empty"
        });
    }

    const filme = new Filme({
        nome: req.body.nome, 
        diretor: req.body.diretor,
        genero: req.body.genero,
        resumo: req.body.resumo
    });

    filme.save()
    .then(filme => {
        res.send(filme);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the film."
        });
    });
};

exports.findAll = (req, res) => {
    Filme.find()
    .then(filmes => {
        res.send(filmes);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving films."
        });
    });
};

exports.findOne = (req, res) => {
    Filme.findById(req.params.id)
    .then(filme => {
        if(!filme) {
            return res.status(404).send({
                message: "Film not found with id " + req.params.id
            });            
        }
        res.send(filme);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Film not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error retrieving film with id " + req.params.id
        });
    });
};

exports.partialUpdate = (req, res) =>{
   if(!req.body){
       return res.status(400).send({
           message: "Film content can not be empty!"
       })
    };
    
    Filme.findByIdAndUpdate(req.params.id, {
        $set: req.body
     },{ new : true , runValidators: true })
     .then(filme => {
         if(!filme){
             return res.status(404).sent({
                 message: "Film not found with id " + req.params.id
             });
         }
         res.send(filme);
      }).catch(err => {
          if(err.kind === 'ObjectId') {
              return res.status(404).send({
                  message: "Film not found with id " + req.params.id
              });                
          }
          return res.status(500).send({
              message: "Error updating film with id " + req.params.id
          });
      });
}

exports.update = (req, res) => {
    if(!req.body) {
        return res.status(400).send({
            message: "Film content can not be empty"
        });
    }


    Filme.findByIdAndUpdate(req.params.id, {
        nome: req.body.nome,
        diretor: req.body.diretor,
        genero: req.body.genero, 
        resumo: req.body.resumo
    },  {new: true, runValidators: true})
    .then(filme => {
        
        if(!filme) {
            return res.status(404).send({
                message: "Film not found with id " + req.params.id
            });
        }
        res.send(filme);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Film not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error updating film with id " + req.params.id
        });
    });
};

exports.delete = (req, res) => {
    Filme.findByIdAndRemove(req.params.id)
    .then(filme => {
        if(!filme) {
            return res.status(404).send({
                message: "Film not found with id " + req.params.id
            });
        }
        res.send({message: "Film deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Film not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Could not delete film with id " + req.params.id
        });
    });
};