const mongoose = require('mongoose')
const { Model, Schema } = mongoose


const Filme = new mongoose.Schema({
  nome:  { type: String, required : true, maxlenght: 40 },
  diretor: { type: String, required: true , maxlenght: 30 },
  genero: { type: String, maxlength: 500 },
  resumo: { type: String, maxlength: 200 }
})


module.exports = mongoose.model("Filme", Filme);